from django.db import models
from django.urls import reverse


class BinVO(models.Model):
    name = models.CharField(max_length=200)
    import_href = models.CharField(max_length=200, unique=True)


# Create your models here.
class Shoe(models.Model):
    manufacturer = models.CharField(max_length=200)
    name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    picture_url = models.URLField(max_length=200, null=True)

    bin = models.ForeignKey(
        BinVO,
        related_name="shoes",
        on_delete=models.CASCADE,
    )

    def get_api_url(self):
        return reverse("", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.manufacturer} - {self.name}"

    class Meta:
        ordering = ("manufacturer", "name")  # Default ordering for Conference
