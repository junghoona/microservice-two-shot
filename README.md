# Wardrobify

Team:

* John - Shoes
* Brandon - Hats

## Design

## Shoes microservice

1. Add shoes api configuration into INSTALLED_APPS under settings.py


Explain your models and integration with the wardrobe
microservice, here.


## Hats microservice

Explain your models and integration with the wardrobe
microservice, here.

The Hat Model and location model will work together in a one to many relationship. Where there can be many Hats to one Location.
The models and wardrobe microservice will be integrated into an SPA using React and RESTful APIs.
