import React, {useEffect, useState} from 'react';


function ShoeColumn(props) {
  const handleDelete = async (shoeId) => {
    try {
      const response = await fetch(`http://localhost:8080/api/shoes/${shoeId}/`, {
        method: "DELETE",
        headers: {
          'Content-Type': 'application/json',
        },
      });
      console.log(response);
      if (response.ok) {
        const data = await response.json();
        console.log(data);
      } else {
        console.error('Error deleting shoe:', response.status);
      }
    } catch (error) {
      console.error('ERROR: ', error);
    }
  }
  return (
    <div className="col">
      {props.list.map(data => {
        console.log('DATA: ', data);
        const bin = data.bin;

        return (
          <div key={bin.href} className="card mb-3 shadow">
            <img src={data.picture_url} className="card-img-top" />
            <div className="card-body">
              <h5 className="card-title">{data.manufacturer}</h5>
              <h6 className="card-subtitle mb-2 text-muted">
                {data.name}
              </h6>
              <p className="card-text">
                {data.bin.name}
              </p>
              <p className="card-text">
                {data.color}
              </p>
            </div>
            <div className="card-footer">
              <button onClick={() => handleDelete(data.id)} type="button" className="btn btn-sm">Delete</button>
            </div>
          </div>
        );
      })}
    </div>
  )
}

const ShoesList = (props) => {
  const [ShoeColumns, setShoeColumns] = useState([[], [], []]);

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/shoes/';

    try {
      const response = await fetch(url);
      if (response.ok) {
        // Get the list of shoes
        const data = await response.json();

        // Create a list for all the requests and 
        // add all of the requests to it
        const requests = [];
        for (let shoe of data.shoes) {
          const detailUrl = `http://localhost:8080/api/shoes/${shoe.id}`;
          requests.push(fetch(detailUrl));
        }

        // Wait for all of the requests to finish simultaneously
        const responses = await Promise.all(requests);

        // Set up the "columns" to put the shoe information into
        const columns = [[], [], []];

        // Loop over the shoe detail responses and add each
        // to the proper "column" if the response is ok
        let i = 0;
        for (const shoeResponse of responses) {
          if (shoeResponse.ok) {
            const details = await shoeResponse.json();
            columns[i].push(details);
            i = i + 1;
            if (i > 2) {
              i = 0;
            }
          } else {
            console.error(shoeResponse);
          }
        }

        // set the state to the new list of three lists of shoes
        setShoeColumns(columns);
      }
    } catch(e) {
      console.error(e);
    }
  }

  useEffect(() => {
    fetchData();

  }, []); 

  return (
    <>
    <div className="container">
    <h2>Registered Shoes</h2>
    <div className="row">
      {ShoeColumns.map((shoeList, index) => {
        return (
          <ShoeColumn key={index} list={shoeList} />
        );
      })}
    </div>
  </div>
  </>
  );
}


export default ShoesList;