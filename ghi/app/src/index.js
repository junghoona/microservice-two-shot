import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App';

// gets the root element in the index.html
const root = ReactDOM.createRoot(document.getElementById('root'));

// async function to get shoes data
async function loadShoes() {
  const response = await fetch('http://localhost:8080/api/shoes/');
  console.log(response);
  if (response.ok) {
    // Gets shoe data
    const data = await response.json();
    console.log('DATA: ', data);
    // Using shoe data
    root.render(
      <React.StrictMode>
        <App shoes={data.shoes} />
      </React.StrictMode>
    );
  } else {
    console.error(response);
  }
}
loadShoes();
