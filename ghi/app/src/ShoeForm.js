import React, { useEffect, useState } from 'react';

function ShoeForm () {
    // useState hook that creates a variable bins, and a method setBins
    const [bins, setBins] = useState([]);  

    // Set the useState hook to store "name" in the component's state,
    // with a default initial value of an empty string.
    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [color, setColor] = useState('');
    const [picture_url, setPictureUrl] = useState('');
    const [bin, setBin] = useState('');


    const fetchData = async () => {
        const url = 'http://localhost:8100/api/bins/';
        
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            console.log('DATA: ', data);

            setBins(data.bins);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    
    const handleSubmit = async (event) => {
        event.preventDefault();

        // create an empty data object
        const data = {};
        // assign bin values to the key names that back end server is expecting
        data.name = name;
        data.manufacturer = manufacturer;
        data.color = color;
        data.picture_url = picture_url;
        data.bin = bin;

        console.log('DATA: ', data);

        const shoeUrl = 'http://localhost:8080/api/shoes/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(shoeUrl, fetchConfig);
        if (response.ok) {
            const newShoe = await response.json();
            console.log(newShoe);
        }

        setName('');
        setManufacturer('');
        setColor('');
        setPictureUrl('');
        setBin('');
    }

    // Create the handleNameChange method to take what the user inputs
    // into the form and store it in the state's "name" variable.
    const handleNameChange = (event) => {
        const value = event.target.value;
        setName(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handlePictureUrlChange = (event) => {
        const value = event.target.value;
        setPictureUrl(value);
    }

    const handleBinChange = (event) => {
        const value = event.target.value;
        setBin(value);
    }

    return (
    <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Create a new shoe</h1>
                <form onSubmit={handleSubmit} id="create-shoe-form">
                    <div className="form-floating mb-3">
                        <input value={name} onChange={handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" />
                        <label htmlFor="name">Name</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={manufacturer} onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" name="manufacturer" id="manufacturer" className="form-control" />
                        <label htmlFor="manufacturer">Manufacturer</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={color} onChange={handleColorChange} placeholder="Color" required type="text" name="color" id="color" className="form-control" />
                        <label htmlFor="color">Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input value={picture_url} onChange={handlePictureUrlChange} placeholder="Picture" required type="text" name="picture_url" id="picture_url" className="form-control" />
                        <label htmlFor="picture_url">Picture</label>
                    </div>
                    <div className="mb-3">
                        <select value={bin} onChange={handleBinChange} required name="bin" id="bin" className="form-select">
                            <option value="">Choose a bin</option>
                            {bins.map(bin => {
                                return (
                                    <option key={bin.href} value={bin.href}>
                                        {bin.closet_name}
                                    </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-primary">Create</button>
                </form>
            </div>
        </div>
    </div>
    );
}

export default ShoeForm;