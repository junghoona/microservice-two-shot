# from django.shortcuts import render
from common.json import ModelEncoder
from .models import Hat, LocationVO
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

# from django.db.models import ObjectDoesNotExist

# Create your views here.


class LocationVOEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "closet_name",
        "import_href",
        "shelf_number",
        "section_number",
    ]


class HatsDetailEncoder(ModelEncoder):
    model = Hat
    properties = [
        "color",
        "fabric",
        "style_name",
        "picture_url",
        "location",
        "id",
    ]
    encoders = {
        "location": LocationVOEncoder(),
    }


class HatsListEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "picture_url",
        "id",
    ]


from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json


@require_http_methods(["GET", "POST", "PUT"])
def api_list_hats(request, location_vo_id=None):
    if request.method == "GET":
        if location_vo_id is not None:
            hats = Hat.objects.filter(location__id=location_vo_id)
            return JsonResponse({"hats": list(hats)}, encoder=HatsListEncoder)
        else:
            hats = Hat.objects.all()
            return JsonResponse({"hats": list(hats)}, encoder=HatsListEncoder)
    else:  # POST request
        try:
            content = json.loads(request.body)
            print(content)
            location_href = content.get("location_href", None)
            if not location_href:
                return JsonResponse(
                    {"message": "Invalid location_href in request"}, status=400
                )

            location = LocationVO.objects.get(import_href=location_href)

            # Create a new hat object
            hat = Hat(
                color=content.get("color", ""),
                fabric=content.get("fabric", ""),
                style_name=content.get("style_name", ""),
                picture_url=content.get("picture_url", ""),
                location=location,
            )

            # Save the new hat object to the database
            hat.save()

            return JsonResponse(
                {"message": "New hat created successfully."},
                status=201,
            )
        except LocationVO.DoesNotExist:
            return JsonResponse({"message": "Invalid location id"}, status=400)
        except Exception as e:
            return JsonResponse({"message": str(e)}, status=400)


# @require_http_methods(["GET", "POST"])
# def api_list_hats(request, location_vo_id=None):
#     if request.method == "GET":
#         if location_vo_id is not None:
#             hats = Hat.objects.all()
#             return JsonResponse({"hats": hats}, encoder=HatsListEncoder)
#         else:
#             hats = Hat.objects.filter(location=location_vo_id)
#             return JsonResponse({"hats": hats}, encoder=HatsListEncoder)
#     else:
#         content = json.loads(request.body)
#         print(content)
#         try:
#             location_href = content["location_href"]
#             # f"/api/locations/{location_vo_id}/"
#             location = LocationVO.objects.get(import_href=location_href)
#             content["location"] = location
#         except LocationVO.DoesNotExist:
#             return JsonResponse({"message": "Invalid location id"}, status=400)
#         hat = Hat.objects.create(**content)
#         return JsonResponse(
#             hat,
#             encoder=HatsDetailEncoder,
#             safe=False,
#         )


@require_http_methods(["GET", "DELETE"])
def api_show_hat(request, id):
    if request.method == "GET":
        try:
            hat = Hat.objects.get(id=id)
            return JsonResponse(
                hat,
                encoder=HatsDetailEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    if request.method == "DELETE":
        count, _ = Hat.objects.get(id=id).delete()
        return JsonResponse(
            {
                "deleted": count > 0,
            }
        )
