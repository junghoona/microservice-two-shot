from django.db import models

# Create your models here.


class LocationVO(models.Model):
    closet_name = models.CharField(max_length=200)
    shelf_number = models.PositiveSmallIntegerField(null=True)
    section_number = models.PositiveSmallIntegerField(unique=True, null=True)
    import_href = models.CharField(max_length=200, unique=True, null=True)


class Hat(models.Model):
    color = models.CharField(max_length=255)
    fabric = models.CharField(max_length=255)
    style_name = models.CharField(max_length=255)
    picture_url = models.URLField(max_length=255, null=True)

    location = models.ForeignKey(
        LocationVO, related_name="hats", on_delete=models.CASCADE
    )

    def __str__(self):
        return self.style_name
