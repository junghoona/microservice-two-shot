import django
import os
import sys
import time
import json
import requests


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "hats_project.settings")
django.setup()

# Import models from hats_rest, here.
# from hats_rest.models import Something

def poll():
    while True:
        print("Hats poller polling for data")
        try:
            response = requests.get("http://wardrobe-api:8000/api/locations/")
            data = response.json()
            locations = data["locations"]
            # print("DATA: ", data)
            for location in locations:
                LocationVO.objects.update_or_create(
                    import_href=location["href"]
                    defaults={
                        # other VO information#
                        "closet_name": location["closet_name"]
                    },
                )
        except Exception as e:
            print(e, file=sys.stderr)
        time.sleep(60)


if __name__ == "__main__":
    poll()
